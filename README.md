# Wav Generator

Generates mono and stereo [WAV files](https://en.wikipedia.org/wiki/WAV) using [Python's built-in wave module](https://docs.python.org/3/library/wave.html).

The `WavGenerator` constructor takes a duration in milliseconds, a volume ranging from 0 to 1 and a boolean value for stereo usage.

The `signal_at` method must be implemented before generating the WAV file. It takes a time parameter
and should return a numeric value for mono, and a tuple of numeric values (left, right) for stereo.


## Examples

### Generating a 440 Hz wave

This example generates a 440 Hz (A4) sine wave with a duration of 1 second, at half the maximum volume.

```python
import math
from wav_generator import WavGenerator

frequency = 440
generator = WavGenerator(1000, 0.5)
generator.signal_at = lambda t: math.sin(2 * math.pi * frequency * t)
generator.write('mono_test.wav')
```


### Generating a stereo wave

This example generates a stereo file, consisting of a 880 Hz wave on the left hand side and a 440 Hz wave on the right hand side.

```python
import math
from wav_generator import WavGenerator

frequency_left = 880
frequency_right = 440
generator = WavGenerator(1000, stereo=True)
generator.signal_at = lambda t: (math.sin(2 * math.pi * frequency_left * t),
                                 math.sin(2 * math.pi * frequency_right * t))
generator.write('stereo_test.wav')
```


### Generating binaural beats

This example generates a three-second stereo file with frequencies for both ears being slightly different.
As a result, the brain perceives a non-existent third, beating sound.
More information about this effect can be found [on Wikipedia](https://en.wikipedia.org/wiki/Beat_(acoustics)#Binaural_beats).

```python
import math
from wav_generator import WavGenerator

def binaural_beat(t, frequency_left, frequency_right):
    return (math.sin(2 * math.pi * frequency_left * t), math.sin(2 * math.pi * frequency_right * t))

generator = WavGenerator(3000, stereo=True)
generator.signal_at = lambda t: binaural_beat(t, 520, 530)
generator.write('binaural_beat.wav')
```


## Further Reading

For more on wave generation in Python, please refer to Andrew Ippoliti's blog:
http://blog.acipo.com/wave-generation-in-python/

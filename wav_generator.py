import math
import wave
import struct


class WavGenerator:

    def __init__(self, duration, volume=0.5, stereo=False):
        self.duration = duration  # in ms
        self.nb_channels = 2 if stereo else 1
        self.sample_width = 2  # 16 bits
        if self.sample_width == 2:
            self.amplitude = 32767 * volume  # fraction of maximum value

        self.sampling_rate: int = 48000
        # Commonly used in professional equipment.
        # An alternative is 44100 Hz, which is used in audio CD's and MP3.

        self.nb_samples = int(math.ceil(self.sampling_rate * duration / 1000))
        # Duration is rounded up to the next sample.

    def signal_at(self, t):
        raise NotImplementedError('signal_at should return a numeric value for mono,'
                                  ' and a tuple of numeric values (left, right) for stereo.')

    def generate(self):
        try:
            raw_signal = [self.signal_at(x/self.sampling_rate) for x in range(self.nb_samples)]
            if self.nb_channels == 2:
                return [(self.amplify_sample(sample_l), self.amplify_sample(sample_r)) for sample_l, sample_r in raw_signal]
            else:
                return [self.amplify_sample(sample) for sample in raw_signal]
        except TypeError:
            raise TypeError('signal_at should return a numeric value for mono,'
                            ' and a tuple of numeric values (left, right) for stereo.')

    def amplify_sample(self, sample):
        return int(sample * self.amplitude)

    def write(self, file_name):
        with wave.open(file_name, 'wb') as file:
            file.setnchannels(self.nb_channels)
            file.setsampwidth(self.sample_width)
            file.setframerate(self.sampling_rate)
            file.setnframes(self.nb_samples)
            file.setcomptype('NONE', 'not compressed')
            if self.nb_channels == 2:
                for sample_l, sample_r in self.generate():
                    file.writeframesraw(struct.pack('<hh', sample_l, sample_r))
            else:
                for sample in self.generate():
                    file.writeframes(struct.pack('h', sample))
